package br.vitali.relatorio;

import java.io.Serializable;

public class ConfigReport implements Serializable {
	
	public ConfigReport(int id, ReportType type) {
		this.id = id;
		this.type = type;
	}
	
	private int id;
	private ReportType type;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ReportType getType() {
		return type;
	}
	public void setType(ReportType type) {
		this.type = type;
	}
	
	
	@Override
	public String toString() {
		return "ID: "+getId()+" Type: "+getType();
	}
	

}
