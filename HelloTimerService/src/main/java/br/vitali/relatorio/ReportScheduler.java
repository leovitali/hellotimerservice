package br.vitali.relatorio;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import br.dagostini.Agendador;
import br.dagostini.Agendamento;

@Singleton
@Startup
public class ReportScheduler implements Scheduler {

	@Resource
	private TimerService timerService;
	
	@PostConstruct
	@Override
	public void iniciaAgendamentos() {
		ConfigReport relatorio1 = new ConfigReport(1, ReportType.CONTABIL);
		ConfigReport relatorio2 = new ConfigReport(2, ReportType.FINANCEIRO);
		ConfigReport relatorio3 = new ConfigReport(3, ReportType.GERENCIAL);
		ConfigReport relatorio4 = new ConfigReport(4, ReportType.CONTABIL);
		
		agendar(relatorio1);
		agendar(relatorio2);
		agendar(relatorio3);
		agendar(relatorio4);

	}

	@PreDestroy
	@Override
	public void encerraAgendamentos() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reiniciaAgendamentos() {
		// TODO Auto-generated method stub

	}

	@Override
	public void agendar(ConfigReport cfgReport) {
		ScheduleExpression expression = new ScheduleExpression();
		
		expression.second("*/5").minute("*").hour("*");
		
		TimerConfig tconfig = new TimerConfig(cfgReport.toString(), false);
		timerService.createCalendarTimer(expression, tconfig);	
	}
	
	@Timeout
	public void execute(Timer timer) {
		String msg = (String) timer.getInfo();
		System.out.println("Relatorio => " + msg);
	}
	
}
