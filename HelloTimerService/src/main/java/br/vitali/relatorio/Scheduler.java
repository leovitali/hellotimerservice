package br.vitali.relatorio;

import br.vitali.relatorio.ConfigReport;

public interface Scheduler {

	void iniciaAgendamentos();

	void encerraAgendamentos();

	void reiniciaAgendamentos();
	
	void agendar(ConfigReport cfgReport);
}