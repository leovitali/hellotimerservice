package br.vitali.relatorio;

public enum ReportType {
	
	GERENCIAL,
	CONTABIL,
	FINANCEIRO

}
