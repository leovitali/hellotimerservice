package br.dagostini;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import br.vitali.relatorio.ConfigReport;

@Singleton
@Startup
public class AgendadorImpl implements Agendador {

	@Resource
	private TimerService timerService;

	public AgendadorImpl() {
		System.out.println("Chamado construtor!");
		
	}

	@PostConstruct
	@Override
	public void iniciaAgendamentos() {
		System.out.println("Chamado PostConstruct");

		Agendamento ag = new Agendamento();
		ag.setHora("*");
		ag.setMinuto("*/1");
		ag.setSegundo("00");
		ag.setMensagem("Msg 1");

		agendar(ag);
		// ---------------------------
		Agendamento ag2 = new Agendamento();
		ag2.setHora("*");
		ag2.setMinuto("*");
		ag2.setSegundo("*/5");
		ag2.setMensagem("Msg 2");

		agendar(ag2);
		
	}

	@PreDestroy
	@Override
	public void encerraAgendamentos() {
		
		System.out.println("Chamado " + "PreDestroy");
		System.out.println("Encerrando " + "agendamentos.");
		
		Collection<Timer> timers = timerService.getTimers();
		
		if (timers == null) {
			System.out.println("Não " + "há agendamentos " + "para cancelar.");
			
		} else {
			
			for (Timer t : timers) {
				t.cancel();
			}
			
		}
		
	}

	@Override
	public void reiniciaAgendamentos() {
		encerraAgendamentos();
		iniciaAgendamentos();
		
	}

	@Override
	public void agendar(Agendamento ag) {
		
		ScheduleExpression expression = new ScheduleExpression();

		expression //
				.second(ag.getSegundo()) //
				.minute(ag.getMinuto()) //
				.hour(ag.getHora());

		TimerConfig tconfig = new TimerConfig(ag.getMensagem(), false);
		timerService.createCalendarTimer(expression, tconfig);
	}

	@Timeout
	public void execute(Timer timer) {
		String msg = (String) timer.getInfo();
		System.out.println("Chegou a hora" + " do agendamento! " + msg);
	}

	@Override
	public void agendar(ConfigReport cfgReport) {
		// TODO Auto-generated method stub
		
	}
}
