package br.dagostini;

import br.vitali.relatorio.ConfigReport;

public interface Agendador {

	void iniciaAgendamentos();

	void encerraAgendamentos();

	void reiniciaAgendamentos();

	void agendar(Agendamento ag);
	
	void agendar(ConfigReport cfgReport);

}