package br.dagostini;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Tester")
public class Tester extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2031478062617621571L;
	@EJB
	private Agendador agendador;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		try {

			String comando = req.getParameter("comando");

			if (comando != null) {
				
				if (comando.equals("start")) {
					agendador.iniciaAgendamentos();
					
				} else if (comando.equals("stop")) {
					agendador.encerraAgendamentos();
					
				} else if (comando.equals("reset")) {
					agendador.reiniciaAgendamentos();
					
				}
				
			}

			PrintWriter pw = null;
			pw = resp.getWriter();
			pw.println(comando);
			pw.flush();
			pw.close();

		} catch (Exception exc) {
			exc.printStackTrace();
		}
		
	}
	
	
	
	
}
