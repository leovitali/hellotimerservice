package br.dagostini;

import java.io.Serializable;

public class Agendamento implements Serializable {

	private static final long serialVersionUID = -2692982418354341604L;
	
	private String hora;
	private String minuto;
	private String segundo;

	private String mensagem;

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getMinuto() {
		return minuto;
	}

	public void setMinuto(String minuto) {
		this.minuto = minuto;
	}

	public String getSegundo() {
		return segundo;
	}

	public void setSegundo(String segundo) {
		this.segundo = segundo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
